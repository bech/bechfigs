#Programs:
* VIM (Text editor)
* i3 (Window Manager)
* zsh (Shell)
* rxvt-unicode (Terminal)
* google-chrome
* pavucontrol (Sound control)
* pa-applet (Sound applet)
* irssi (IRC client)

#Setup:
1. run home.sh
2. xrdb -merge .Xresources
3. chsh -s /usr/bin/zsh (ubuntu)
4. Install https://github.com/fernandotcl/pa-applet

#Go Development Setup:
Autocomplete:
* go get github.com/nsf/gocode
* go install github.com/nsf/gocode
