[[ -f ~/.zshrc ]] && source ~/.zshrc

#Add bin if it exists
if [ -d "$HOME/bin" ] ; then
    export PATH="$HOME/bin:$PATH"
fi

#Golang
if [ -d "$HOME/projects/go" ] ; then
    export GOPATH=$HOME/projects/go
    export GOBIN=$GOPATH/bin
    export PATH=$PATH:$GOBIN
fi

#Scala play
if [ -d "$HOME/bin/play" ] ; then
    export PATH=$PATH:$HOME/bin/play
fi

#Cabal
if [ -d "$HOME/.cabal/bin" ] ; then
    export PATH=$PATH:$HOME/.cabal/bin
fi

export LD_LIBRARY_PATH=/usr/local/lib
export PATH=$PATH:/usr/local/go/bin

if [ -z "$DISPLAY" ] ; then
        startx
fi

export PATH="/usr/sbin:$PATH"
export PATH="/opt/android-studio/bin:$PATH"
export PATH="/opt/android-studio/sdk/tools:$PATH"
export PATH="/opt/android-studio/sdk/platform-tools:$PATH"
