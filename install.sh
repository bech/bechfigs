#General Software
sudo apt-get update
sudo apt-get install vim tmux zsh rxvt-unicode pavucontrol libglib2.0-dev libgtk-3-dev libnotify-dev libpulse-dev libx11-dev autoconf automake pkg-config
./home.sh
xrdb -merge ~/.Xresources
chsh -s /usr/bin/zsh
#i3 wm
echo "deb http://debian.sur5r.net/i3/ $(lsb_release -c -s) universe" >> /etc/apt/sources.list
sudo apt-get update
sudo apt-get --allow-unauthenticated install sur5r-keyring
sudo apt-get update
sudo apt-get install i3
#Pa-applet
cd ../
git clone https://github.com/fernandotcl/pa-applet
cd pa-applet/
./autogen.sh
./configure --prefix=/usr
make -f ../pa-applet/Makefile
sudo make install -f ../pa-applet/Makefile
#Google Chrome
cd ~/Downloads
sudo apt-get install libxss1
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome*.deb
